#ifndef POST_PROCESS_H
#define POST_PROCESS_H

#include <string>

using namespace std;

int downsample(const string& inFilename,const string& outFilename);
int SED(const string& inFilename,const string& outFilename,const string& modelFilename="model.yml.gz");
int runDeepMatch(const string& in1Filename,const string& in2Filename,const string& matchFilename);
int runFullFlow(const string& in1Filename,const string& in2Filename,const string& matchFilename);
int runEpicFlow(const string& in1Filename,const string& in2Filename,const string& matchFilename,
                const string& out1EdgeFilename,const string& outInterpFilename);
int runPipeline(const string& pathName,int nrDownsample=0,bool fullFlow=false,bool postProcess=true);
void visualize(const string& pathName,bool scaled=true);

#endif
