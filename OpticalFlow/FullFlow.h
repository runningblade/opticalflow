#ifndef FULL_FLOW_H
#define FULL_FLOW_H

#include <Eigen/Dense>
#include <boost/shared_ptr.hpp>

using namespace std;

typedef float MRFT;
class FullFlow
{
public:
  struct Match {
    Match(int r,int c):_r(r),_c(c) {}
    bool operator<(const Match& other) const {
      return _score < other._score;
    }
    int _r,_c;
    float _score;
    Eigen::Vector2i _pFrom,_pTo;
  };
  struct Level {
    Level() {}
    Level(const Eigen::MatrixXf& from);
    Eigen::Vector2i getPatchCoord(int i) const;
    Eigen::Vector2i getPixelCoord(int i) const;
    int getPatchOffset(const Eigen::Vector2i& crd) const;
    int getPixelOffset(const Eigen::Vector2i& crd) const;
    int getPatchOffsetConditional(const Eigen::Vector2i& crd) const;
    int getPixelOffsetConditional(const Eigen::Vector2i& crd) const;
    Eigen::MatrixXf reconstructSimFromChild(const Level& last,float rectify) const;
    vector<Match> backtrace(const Level& last,vector<Match>& index) const;
    void computeSim(const Eigen::MatrixXf fromF[9],const Eigen::MatrixXf toF[9],float rectify);
    void computeSim(const Level& last,float rectify);
    void maxPoolingSimple(int offset);
    void maxPoolingFast(int offset);
    void maxPoolingVert(int offset);
    void maxPoolingHoriz(int offset);
    void downSample();
    void aggregate();
    //debug code
    static void debugMatrixMultiply();
    static void debugMaxPooling();
    static void debugBacktrace();
    //_sim(y,x) is a patch starting at index y*4,x*4
    int _N,_height,_width,_heightTo,_widthTo;
    Eigen::MatrixXi _child[4];
    Eigen::MatrixXf _sim;
  };
  FullFlow();
  FullFlow(const string& fromFilename,const string& toFilename);
  void createImageDesc(Eigen::MatrixXf& I,Eigen::MatrixXf IF[9]);
  void createImageDesc();
  void computePyramid();
  float backtrace(int id,vector<Match>& correspondence) const;
  void backtrace();
  void drawMatch();
  void solveOpticalFlow();
  void writeMatch(const string& path);
  void reset(bool isPng);
  //in/tmp/out
  Eigen::MatrixXf _from,_to;
  Eigen::MatrixXf _fromF[9],_toF[9];
  vector<boost::shared_ptr<Level> > _simLevel;
  vector<Match> _correspondence;
  int _priorDownscale;
  float _presmooth;
  float _midsmooth;
  float _postsmooth;
  float _hogSigmoid;
  float _ninthDim;
  float _rectify;
  float _clampThres;
};

#endif
