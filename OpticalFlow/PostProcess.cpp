#include "PostProcess.h"
#include "FullFlow.h"
#include "RGB2HSV.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/utility.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/lexical_cast.hpp>

int downsample(const string& inFilename,const string& outFilename)
{
  printf("Downsampling image from %s to %s!\n",inFilename.c_str(),outFilename.c_str());
  cv::Mat image=cv::imread(inFilename,1),imageOut;
  if(image.empty()) {
    printf("Cannot read image file: %s!\n",inFilename.c_str());
    return -1;
  }
  cv::Size dsize=image.size()/2;
  cv::resize(image,imageOut,dsize);
  if(outFilename == "") {
    cv::namedWindow("resize",1);
    cv::imshow("resize",imageOut);
    cv::waitKey(0);
  } else cv::imwrite(outFilename,imageOut);
  return 0;
}
int SED(const string& inFilename,const string& outFilename,const string& modelFilename)
{
  using namespace cv::ximgproc;
  printf("SED edge detection image from %s to %s!\n",inFilename.c_str(),outFilename.c_str());
  cv::Mat image=cv::imread(inFilename,1);
  if(image.empty()) {
    printf("Cannot read image file: %s!\n",inFilename.c_str());
    return -1;
  }
  image.convertTo(image,cv::DataType<float>::type,1/255.0);
  cv::Mat edges(image.size(),image.type());
  cv::Ptr<StructuredEdgeDetection> pDollar=createStructuredEdgeDetection(modelFilename);
  pDollar->detectEdges(image,edges);
  edges=1-edges;
  if(outFilename == "") {
    cv::namedWindow("edges",1);
    cv::imshow("edges", edges);
    cv::waitKey(0);
  } else {
    cv::imwrite(outFilename,255*edges);
    boost::filesystem::path path=boost::filesystem::path(outFilename).replace_extension(".edge");
    boost::filesystem::ofstream os(path,ios::binary);
    for(int y=0; y<edges.size().height; y++)
      for(int x=0; x<edges.size().width; x++) {
        float pixel=edges.template at<float>(y,x);
        os.write((const char*)&pixel,sizeof(float));
      }
  }
  return 0;
}
int runDeepMatch(const string& in1Filename,const string& in2Filename,const string& matchFilename)
{
  string command="./deepmatching-static "+
                 in1Filename+" "+
                 in2Filename+" -out "+
                 matchFilename;
  printf("Running deepmatching-static:\n %s\n",command.c_str());
  int ret=system(command.c_str());
  return ret;
}
int runFullFlow(const string& in1Filename,const string& in2Filename,const string& matchFilename)
{
  FullFlow flow(in1Filename,in2Filename);
  flow.solveOpticalFlow();
  flow.writeMatch(matchFilename);
  return 0;
}
int runEpicFlow(const string& in1Filename,const string& in2Filename,const string& matchFilename,
                const string& out1EdgeFilename,const string& outInterpFilename)
{
  string command="./epicflow-static "+
                 in1Filename+" "+
                 in2Filename+" "+
                 out1EdgeFilename+" "+
                 matchFilename+" "+
                 outInterpFilename;
  printf("Running epicflow-static:\n %s\n",command.c_str());
  int ret=system(command.c_str());
  return ret;
}
int runPipeline(const string& pathName,int nrDownsample,bool fullFlow,bool postProcess)
{
  //file check
  bool hasA=false,hasB=false;
  for(boost::filesystem::directory_iterator beg(pathName),end; beg!=end; beg++) {
    boost::filesystem::path path=*beg;
    string name=path.filename().string();
    //printf("%s",name.c_str());
    if(name == "A.png")
      hasA=true;
    else if(name == "B.png")
      hasB=true;
  }
  if(!hasA) {
    printf("Cannot find image %s/A.XXX!",pathName.c_str());
    return -1;
  }
  if(!hasB) {
    printf("Cannot find image %s/B.XXX!",pathName.c_str());
    return -1;
  }
  //image size adjustment
  string from="",to="";
  for(int i=0; i<nrDownsample; i++) {
    from=i == 0 ? "" : boost::lexical_cast<string>(i);
    to=boost::lexical_cast<string>(i+1);
    downsample(pathName+"/A"+from+".png",
               pathName+"/A"+to+".png");
    downsample(pathName+"/B"+from+".png",
               pathName+"/B"+to+".png");
  }
  //run deep match
  if(fullFlow)
    runFullFlow(pathName+"/A"+to+".png",
                pathName+"/B"+to+".png",
                pathName+"/match.out");
  else runDeepMatch(pathName+"/A"+to+".png",
                      pathName+"/B"+to+".png",
                      pathName+"/match.out");
  if(postProcess) {
    //run edge detect
    SED(pathName+"/A"+to+".png",
        pathName+"/Edge.png");
    //run EpicFlow
    runEpicFlow(pathName+"/A"+to+".png",
                pathName+"/B"+to+".png",
                pathName+"/match.out",
                pathName+"/Edge.edge",
                pathName+"/Interp.out");
  }
  return 0;
}
void visualizeInner(const string& inFilename,const string& matchFilename,const string& outFilename,bool scaled)
{
  string line;
  double length=0;
  int x0,y0,x1,y1;
  vector<cv::Vec4i> match;
  cv::Mat image=cv::imread(inFilename,1);
  cv::Mat imageOut(image.size().height,image.size().width,CV_8UC3,cv::Vec3b(0,0,0));
  boost::filesystem::ifstream is(matchFilename);
  while(!is.eof()) {
    getline(is,line);
    istringstream(line) >> x0 >> y0 >> x1 >> y1;
    match.push_back(cv::Vec4i(x0,y0,x1,y1));
    length=max<double>(length,cv::norm(cv::Vec2d(x1-x0,y1-y0)));
  }
  for(int i=0; i<(int)match.size(); i++) {
    x0=match[i][0];
    y0=match[i][1];
    x1=match[i][2];
    y1=match[i][3];
    double angle=atan2(y1-y0,x1-x0);
    if(angle < 0)
      angle+=M_PI*2;
    angle*=180.0f/M_PI;

    hsv chsv;
    chsv.h=angle;
    chsv.v=1;
    chsv.s=scaled ? cv::norm(cv::Vec2d(x1-x0,y1-y0))/length : 1;
    rgb crgb=hsv2rgb(chsv);

    cv::Vec3b& c=imageOut.template at<cv::Vec3b>(y0,x0);
    c[0]=(unsigned char)(255*crgb.b);
    c[1]=(unsigned char)(255*crgb.g);
    c[2]=(unsigned char)(255*crgb.r);
  }
  cv::imwrite(outFilename,imageOut);
}
void visualizeInnerBinary(const string& inFilename,const string& matchFilename,const string& outFilename,bool scaled)
{
  float help,x1,y1;
  double length=0;
  int x0,y0,width,height;
  vector<pair<cv::Vec2i,cv::Vec2d> > match;
  cv::Mat image=cv::imread(inFilename,1);
  cv::Mat imageOut(image.size().height,image.size().width,CV_8UC3,cv::Vec3b(0,0,0));
  boost::filesystem::ifstream is(matchFilename,ios::binary);
  is.read((char*)&help,sizeof(float));
  is.read((char*)&width,sizeof(int));
  is.read((char*)&height,sizeof(int));
  for(y0=0; y0<image.size().height; y0++)
    for(x0=0; x0<image.size().width; x0++) {
      is.read((char*)&x1,sizeof(float));
      is.read((char*)&y1,sizeof(float));
      match.push_back(make_pair(cv::Vec2i(x0,y0),cv::Vec2d(x1,y1)));
      length=max<double>(length,cv::norm(cv::Vec2d(x1,y1)));
    }
  for(int i=0; i<(int)match.size(); i++) {
    x0=match[i].first[0];
    y0=match[i].first[1];
    x1=match[i].second[0];
    y1=match[i].second[1];
    double angle=atan2(y1,x1);
    if(angle < 0)
      angle+=M_PI*2;
    angle*=180.0f/M_PI;

    hsv chsv;
    chsv.h=angle;
    chsv.v=1;
    chsv.s=scaled ? cv::norm(cv::Vec2d(x1,y1))/length : 1;
    rgb crgb=hsv2rgb(chsv);

    cv::Vec3b& c=imageOut.template at<cv::Vec3b>(y0,x0);
    c[0]=(unsigned char)(255*crgb.b);
    c[1]=(unsigned char)(255*crgb.g);
    c[2]=(unsigned char)(255*crgb.r);
  }
  cv::imwrite(outFilename,imageOut);
}
void visualize(const string& pathName,bool scaled)
{
  boost::filesystem::path path;
  for(int i=0;; i++) {
    string to=i == 0 ? "" : boost::lexical_cast<string>(i);
    string name=pathName+"/A"+to+".png";
    if(boost::filesystem::exists(name))
      path=name;
    else break;
  }
  assert(!path.string().empty());
  printf("Visualizing: %s\n!",path.string().c_str());
  if(boost::filesystem::exists(pathName+"/match.out"))
    visualizeInner(path.string(),pathName+"/match.out",pathName+"/visualizeMatch.png",scaled);
  if(boost::filesystem::exists(pathName+"/Interp.out"))
    visualizeInnerBinary(path.string(),pathName+"/Interp.out",pathName+"/visualizeInterp.png",scaled);
}
