#include "FullFlow.h"
#include "RGB2HSV.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/utility.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/lexical_cast.hpp>

extern "C" {
  int sgemm_(char *transa, char *transb, int *m, int *n, int *k,
             float *alpha, float *a, int *lda, float *b, int *ldb,
             float *beta, float *c, int *ldc);
}
float pow2(float val)
{
  return val*val;
}
void copy(const cv::Mat& image,Eigen::MatrixXf& imageMat)
{
  imageMat.resize(image.size().height,image.size().width);
  for(int y=0; y<image.size().height; y++)
    for(int x=0; x<image.size().width; x++) {
      const cv::Vec3b& c=image.template at<cv::Vec3b>(y,x);
      imageMat(y,x)=((float)c[0]+(float)c[1]+(float)c[2])/3;
    }
}
void sigmoid(Eigen::MatrixXf& M,float coef,float offset)
{
  for(int y=0; y<M.rows(); y++)
    for(int x=0; x<M.cols(); x++)
      M(y,x)=2.f/(1.f+exp(-coef*M(y,x)+offset))-1.f;
}
void smooth(Eigen::MatrixXf& M,float sigma)
{
  float coef[7]= {
    exp(-pow2(3./sigma)/2),
    exp(-pow2(2./sigma)/2),
    exp(-pow2(1./sigma)/2),
    exp(-pow2(0./sigma)/2),
    exp(-pow2(1./sigma)/2),
    exp(-pow2(2./sigma)/2),
    exp(-pow2(3./sigma)/2),
  };
  float sum=0;
  for(int i=0; i<7; i++)
    sum+=coef[i];
  for(int i=0; i<7; i++)
    coef[i]/=sum;
  Eigen::MatrixXf tmp=M;
  for(int y=0; y<M.rows(); y++)
    for(int x=0; x<M.cols(); x++) {
      float& val=tmp(y,x)=0;
      for(int dx=-3; dx<=3; dx++)
        val+=coef[dx+3]*M(y,max<int>(min<int>(x+dx,M.cols()-1),0));
    }
  for(int y=0; y<M.rows(); y++)
    for(int x=0; x<M.cols(); x++) {
      float& val=M(y,x)=0;
      for(int dy=-3; dy<=3; dy++)
        val+=coef[dy+3]*tmp(max<int>(min<int>(y+dy,M.rows()-1),0),x);
    }
}
void downSampleAvg(Eigen::MatrixXf& M)
{
  assert(M.rows()%2 == 0 && M.cols()%2 == 0);
  Eigen::MatrixXf ret(M.rows()/2,M.cols()/2);
  for(int y=0; y<ret.rows(); y++)
    for(int x=0; x<ret.cols(); x++)
      ret(y,x)=(M(y*2,x*2)+M(y*2+1,x*2)+M(y*2,x*2+1)+M(y*2+1,x*2+1))/4;
  M=ret;
}
Eigen::MatrixXf gradX(Eigen::MatrixXf& M)
{
  Eigen::MatrixXf ret=M;
  for(int y=0; y<M.rows(); y++)
    for(int x=0; x<M.cols(); x++)
      if(x>0 && x<M.cols()-1)
        ret(y,x)=M(y,x+1)-M(y,x-1);
      else if(x == 0)
        ret(y,x)=M(y,x+1)-M(y,x);
      else ret(y,x)=M(y,x)-M(y,x-1);
  return ret;
}
Eigen::MatrixXf gradY(Eigen::MatrixXf& M)
{
  Eigen::MatrixXf ret=M;
  for(int y=0; y<M.rows(); y++)
    for(int x=0; x<M.cols(); x++)
      if(y>0 && y<M.rows()-1)
        ret(y,x)=M(y+1,x)-M(y-1,x);
      else if(y == 0)
        ret(y,x)=M(y+1,x)-M(y,x);
      else ret(y,x)=M(y,x)-M(y-1,x);
  return ret;
}
void HOG(const Eigen::MatrixXf& IX,const Eigen::MatrixXf& IY,Eigen::MatrixXf& IF,int i,int nOri)
{
  float angle=-2*(i-2)*M_PI/nOri;
  float kos=cos(angle);
  float zin=sin(angle);
  IF=IX;
  for(int y=0; y<IF.rows(); y++)
    for(int x=0; x<IF.cols(); x++)
      IF(y,x)=max<float>(IX(y,x)*zin+IY(y,x)*kos,0);
}
cv::Mat writeImage(const Eigen::MatrixXf& M,float coef=1,bool color=false)
{
  cv::Mat ret=cv::Mat(M.rows(),M.cols(),color ? CV_8UC3 : CV_8UC1);
  for(int y=0; y<ret.size().height; y++)
    for(int x=0; x<ret.size().width; x++) {
      float val=M(y,x)*coef;
      if(color)
        ret.template at<cv::Vec3b>(y,x)=cv::Vec3b(val,val,val);
      else ret.template at<uchar>(y,x)=val;
    }
  return ret;
}
void writeImage(const Eigen::MatrixXf& M,const string& path,float coef=1)
{
  cv::Mat ret=writeImage(M,coef);
  cv::imwrite(path,ret);
}
Eigen::Matrix<float,16*9,1> fillBlock(const Eigen::Vector2i& crd,const Eigen::MatrixXf* features)
{
  int rows=features[0].rows();
  int cols=features[0].cols();
  Eigen::Matrix<float,16*9,1> ret;
  for(int x=crd[0],k=0; x<crd[0]+4; x++)
    for(int y=crd[1]; y<crd[1]+4; y++)
      for(int z=0; z<9; z++)
        ret[k++]=(y<rows && x<cols) ? features[z](y,x) : 0;
  return ret/max<float>(1E-6f,ret.norm());
}
void matMulFast(const Eigen::MatrixXf& PFrom,const Eigen::MatrixXf& PTo,Eigen::MatrixXf& sim)
{
  int m=PFrom.rows();
  int n=PTo.cols();
  int k=PFrom.cols();
  float alpha=1,beta=0;
  sim.setZero(PFrom.rows(),PTo.cols());
  char trans='N';
  sgemm_(&trans,&trans,&m,&n,&k,
         &alpha,(float*)PFrom.data(),&m,(float*)PTo.data(),&k,
         &beta,(float*)sim.data(),&m);
}
void rectifySim(Eigen::MatrixXf& sim,float rectify)
{
  float* dat=sim.data();
  #pragma omp parallel for
  for(int i=0; i<sim.size(); i++)
    dat[i]=pow(dat[i],rectify);
}
void makePOT(Eigen::MatrixXf& img)
{
  int r=1,c=1;
  while(r<img.rows())
    r<<=1;
  while(c<img.cols())
    c<<=1;
  Eigen::MatrixXf tmp;
  tmp.setZero(r,c);
  tmp.block(0,0,img.rows(),img.cols())=img;
  img.swap(tmp);
}
//Level
FullFlow::Level::Level(const Eigen::MatrixXf& from)
{
  _N=4;
  _height=from.rows()/4;
  _width=from.cols()/4;
  _heightTo=from.rows();
  _widthTo=from.cols();
}
Eigen::Vector2i FullFlow::Level::getPatchCoord(int i) const
{
  Eigen::Vector2i ret=Eigen::Vector2i(i/_width,i%_width);
  assert(ret[0] < _height);
  return ret;
}
Eigen::Vector2i FullFlow::Level::getPixelCoord(int j) const
{
  Eigen::Vector2i ret(j/_widthTo,j%_widthTo);
  assert(ret[0] < _heightTo);
  return ret;
}
int FullFlow::Level::getPatchOffset(const Eigen::Vector2i& crd) const
{
  assert(crd[0] < _height && crd[1] < _width);
  return crd[0]*_width+crd[1];
}
int FullFlow::Level::getPixelOffset(const Eigen::Vector2i& crd) const
{
  assert(crd[0] < _heightTo && crd[1] < _widthTo);
  return crd[0]*_widthTo+crd[1];
}
int FullFlow::Level::getPatchOffsetConditional(const Eigen::Vector2i& crd) const
{
  if(crd[0] < _height && crd[1] < _width)
    return crd[0]*_width+crd[1];
  else return -1;
}
int FullFlow::Level::getPixelOffsetConditional(const Eigen::Vector2i& crd) const
{
  if(crd[0] < _heightTo && crd[1] < _widthTo)
    return crd[0]*_widthTo+crd[1];
  else return -1;
}
Eigen::MatrixXf FullFlow::Level::reconstructSimFromChild(const Level& last,float rectify) const
{
  Eigen::MatrixXf ret;
  ret.setZero(_sim.rows(),_sim.cols());
  for(int r=0; r<ret.rows(); r++) {
    Eigen::Vector2i crdPatch=getPatchCoord(r);
    int offPatch[4]= {
      last.getPatchOffsetConditional(crdPatch+Eigen::Vector2i(0,0)),
      last.getPatchOffsetConditional(crdPatch+Eigen::Vector2i(_N/8,0)),
      last.getPatchOffsetConditional(crdPatch+Eigen::Vector2i(0,_N/8)),
      last.getPatchOffsetConditional(crdPatch+Eigen::Vector2i(_N/8,_N/8)),
    };
    for(int c=0; c<ret.cols(); c++) {
      int weight=0;
      float& val=ret(r,c);
      for(int d=0; d<4; d++)
        if(offPatch[d] >= 0 && _child[d].size() > 0 && _child[d](r,c) >= 0) {
          val+=last._sim(offPatch[d],_child[d](r,c));
          weight+=1;
        }
      val/=(float)weight;
      assert(weight >= 1);
    }
  }
  rectifySim(ret,rectify);
  return ret;
}
vector<FullFlow::Match> FullFlow::Level::backtrace(const Level& last,vector<Match>& index) const
{
  //backtrace
  vector<Match> ret;
  for(int i=0; i<(int)index.size(); i++) {
    Eigen::Vector2i crdPatch=getPatchCoord(index[i]._r);
    int offPatch[4]= {
      last.getPatchOffsetConditional(crdPatch+Eigen::Vector2i(0,0)),
      last.getPatchOffsetConditional(crdPatch+Eigen::Vector2i(_N/8,0)),
      last.getPatchOffsetConditional(crdPatch+Eigen::Vector2i(0,_N/8)),
      last.getPatchOffsetConditional(crdPatch+Eigen::Vector2i(_N/8,_N/8)),
    };
    for(int d=0; d<4; d++)
      if(offPatch[d] >= 0 && _child[d](index[i]._r,index[i]._c) >= 0)
        ret.push_back(Match(offPatch[d],_child[d](index[i]._r,index[i]._c)));
  }
  return ret;
}
void FullFlow::Level::computeSim(const Eigen::MatrixXf fromF[9],const Eigen::MatrixXf toF[9],float rectify)
{
  assert(_N == 4);
  Eigen::MatrixXf PFrom(_width*_height,16*9);
  Eigen::MatrixXf PTo(16*9,toF[0].rows()*toF[0].cols());
  #pragma omp parallel for
  for(int i=0; i<PFrom.rows(); i++) {
    Eigen::Vector2i crd=getPatchCoord(i)*_N;
    PFrom.row(i)=fillBlock(crd,fromF);
  }
  #pragma omp parallel for
  for(int i=0; i<PTo.cols(); i++) {
    Eigen::Vector2i crd=getPixelCoord(i);
    PTo.col(i)=fillBlock(crd,toF);
  }
#define USE_BLAS
#ifdef USE_BLAS
  matMulFast(PFrom,PTo,_sim);
#else
  _sim=PFrom*PTo;
#endif
  rectifySim(_sim,rectify);
}
void FullFlow::Level::computeSim(const Level& last,float rectify)
{
  _child[0].resize(_sim.rows(),_sim.cols());
  _child[1].resize(0,0);
  _child[2].resize(0,0);
  _child[3].resize(0,0);
  for(int c=0; c<_child[0].cols(); c++)
    _child[0].col(c).setConstant(c);
  maxPoolingFast(1);
  downSample();
  aggregate();
  rectifySim(_sim,rectify);
}
void FullFlow::Level::maxPoolingSimple(int offset)
{
  Eigen::MatrixXf tmp=_sim;
  Eigen::MatrixXi tmpc=_child[0];
  #pragma omp parallel for
  for(int r=0; r<_sim.rows(); r++)
    for(int y=0; y<_heightTo; y++)
      for(int x=0; x<_widthTo; x++) {
        int c=getPixelOffset(Eigen::Vector2i(y,x));
        float& val=_sim(r,c);
        int& valc=_child[0](r,c);
        int y0=max<int>(y-offset,0);
        int y1=min<int>(y+offset,_heightTo-1);
        int x0=max<int>(x-offset,0);
        int x1=min<int>(x+offset,_widthTo-1);
        for(int yy=y0; yy<=y1; yy++)
          for(int xx=x0; xx<=x1; xx++) {
            c=getPixelOffset(Eigen::Vector2i(yy,xx));
            if(tmp(r,c) > val) {
              val=tmp(r,c);
              valc=tmpc(r,c);
            }
          }
      }
}
void FullFlow::Level::maxPoolingFast(int offset)
{
  maxPoolingVert(offset);
  maxPoolingHoriz(offset);
}
void FullFlow::Level::maxPoolingVert(int offset)
{
  #pragma omp parallel for
  for(int r=0; r<_sim.rows(); r++) {
    int c=-1;
    for(int y=0; y<_heightTo; y++) {
      int y1=min<int>(y+offset,_heightTo-1);
      for(int x=0; x<_widthTo; x++) {
        c=getPixelOffset(Eigen::Vector2i(y,x));
        float& val=_sim(r,c);
        int& valc=_child[0](r,c);
        for(int yy=y; yy<=y1; yy++) {
          c=getPixelOffset(Eigen::Vector2i(yy,x));
          if(_sim(r,c) > val) {
            val=_sim(r,c);
            valc=_child[0](r,c);
          }
        }
      }
    }
    for(int y=_heightTo-1; y>=0; y--) {
      int y0=max<int>(y-offset,0);
      for(int x=0; x<_widthTo; x++) {
        c=getPixelOffset(Eigen::Vector2i(y,x));
        float& val=_sim(r,c);
        int& valc=_child[0](r,c);
        for(int yy=y0; yy<=y; yy++) {
          c=getPixelOffset(Eigen::Vector2i(yy,x));
          if(_sim(r,c) > val) {
            val=_sim(r,c);
            valc=_child[0](r,c);
          }
        }
      }
    }
  }
}
void FullFlow::Level::maxPoolingHoriz(int offset)
{
  #pragma omp parallel for
  for(int r=0; r<_sim.rows(); r++) {
    int c=-1;
    for(int x=0; x<_widthTo; x++) {
      int x1=min<int>(x+offset,_widthTo-1);
      for(int y=0; y<_heightTo; y++) {
        c=getPixelOffset(Eigen::Vector2i(y,x));
        float& val=_sim(r,c);
        int& valc=_child[0](r,c);
        for(int xx=x; xx<=x1; xx++) {
          c=getPixelOffset(Eigen::Vector2i(y,xx));
          if(_sim(r,c) > val) {
            val=_sim(r,c);
            valc=_child[0](r,c);
          }
        }
      }
    }
    for(int x=_widthTo-1; x>=0; x--) {
      int x0=max<int>(x-offset,0);
      for(int y=0; y<_heightTo; y++) {
        c=getPixelOffset(Eigen::Vector2i(y,x));
        float& val=_sim(r,c);
        int& valc=_child[0](r,c);
        for(int xx=x0; xx<=x; xx++) {
          c=getPixelOffset(Eigen::Vector2i(y,xx));
          if(_sim(r,c) > val) {
            val=_sim(r,c);
            valc=_child[0](r,c);
          }
        }
      }
    }
  }
}
void FullFlow::Level::downSample()
{
  Level tmp;
  tmp._heightTo=_heightTo/2;
  tmp._widthTo=_widthTo/2;
  tmp._sim.resize(_sim.rows(),tmp._heightTo*tmp._widthTo);
  tmp._child[0]=_child[0];
  for(int r=0; r<tmp._sim.rows(); r++)
    for(int c=0; c<tmp._sim.cols(); c++) {
      Eigen::Vector2i crd=tmp.getPixelCoord(c)*2;
      int offset=getPixelOffset(crd);
      tmp._sim(r,c)=_sim(r,offset);
      tmp._child[0](r,c)=_child[0](r,offset);
    }
  //assign
  _heightTo=tmp._heightTo;
  _widthTo=tmp._widthTo;
  _sim=tmp._sim;
  _child[0]=tmp._child[0];
}
void FullFlow::Level::aggregate()
{
  int nrPatchOff=_N/4-1;
  int nrPatchHeightBase=_height+nrPatchOff;
  int nrPatchWidthBase=_width+nrPatchOff;

  Level tmp;
  tmp._N=_N*2;
  int nrPatchOffParent=tmp._N/4-1;
  tmp._height=max<int>(nrPatchHeightBase-nrPatchOffParent,1);
  tmp._width=max<int>(nrPatchWidthBase-nrPatchOffParent,1);
  tmp._heightTo=_heightTo;
  tmp._widthTo=_widthTo;
  tmp._sim.resize(tmp._height*tmp._width,tmp._heightTo*tmp._widthTo);
  for(int d=0; d<4; d++)
    tmp._child[d].setConstant(tmp._sim.rows(),tmp._sim.cols(),-1);
  #pragma omp parallel for
  for(int r=0; r<tmp._sim.rows(); r++) {
    Eigen::Vector2i crdPatch=tmp.getPatchCoord(r);
    int offPatch[4]= {
      getPatchOffsetConditional(crdPatch+Eigen::Vector2i(0,0)),
      getPatchOffsetConditional(crdPatch+Eigen::Vector2i(_N/4,0)),
      getPatchOffsetConditional(crdPatch+Eigen::Vector2i(0,_N/4)),
      getPatchOffsetConditional(crdPatch+Eigen::Vector2i(_N/4,_N/4)),
    };
    for(int c=0; c<tmp._sim.cols(); c++) {
      int weight=0;
      float& val=tmp._sim(r,c)=0;
      Eigen::Vector2i crdPixel=tmp.getPixelCoord(c);
      int offPixel[4]= {
        getPixelOffsetConditional(crdPixel+Eigen::Vector2i(0,0)),
        getPixelOffsetConditional(crdPixel+Eigen::Vector2i(2,0)),
        getPixelOffsetConditional(crdPixel+Eigen::Vector2i(0,2)),
        getPixelOffsetConditional(crdPixel+Eigen::Vector2i(2,2)),
      };
      for(int d=0; d<4; d++)
        if(offPatch[d] >= 0 && offPixel[d] >= 0) {
          val+=_sim(offPatch[d],offPixel[d]);
          tmp._child[d](r,c)=_child[0](offPatch[d],offPixel[d]);
          weight+=1;
        }
      assert(weight >= 1);
      val/=(float)weight;
    }
  }
  *this=tmp;
}
//debug code
void FullFlow::Level::debugMatrixMultiply()
{
  Eigen::MatrixXf A,B,C,C2;
  A.setRandom(10,20);
  B.setRandom(20,30);
  matMulFast(A,B,C);
  C2=A*B;
  printf("MatMul Norm: %f Err: %f!\n",C.norm(),(C-C2).norm());
}
void FullFlow::Level::debugMaxPooling()
{
  Level test,test2;
  test._height=30;
  test._width=20;
  test._heightTo=40;
  test._widthTo=10;
  test._sim.setRandom(test._height*test._width,test._heightTo*test._widthTo);
  test._child[0].resize(test._sim.rows(),test._sim.cols());
  for(int c=0; c<test._child[0].cols(); c++)
    test._child[0].col(c).setConstant(c);
  test2=test;

  test.maxPoolingFast(1);
  test2.maxPoolingSimple(1);
  printf("MaxPooling Norm: %f Err: %f!\n",test._sim.cwiseAbs().maxCoeff(),(test._sim-test2._sim).cwiseAbs().maxCoeff());
  printf("MaxPooling ChildNorm: %d Err: %d!\n",test._child[0].cwiseAbs().maxCoeff(),(test._child[0]-test2._child[0]).cwiseAbs().maxCoeff());
}
void FullFlow::Level::debugBacktrace()
{
  Level test,test2;
  test._N=16;
  test._height=30;
  test._width=20;
  test._heightTo=40;
  test._widthTo=10;
  test._sim.setRandom(test._height*test._width,test._heightTo*test._widthTo);
  test._child[0].resize(test._sim.rows(),test._sim.cols());

  test2=test;
  test2.computeSim(test,1.6f);
  Eigen::MatrixXf reconSim=test2.reconstructSimFromChild(test,1.6f);
  printf("ReconSim Norm: %f Err: %f!\n",reconSim.cwiseAbs().maxCoeff(),(reconSim-test2._sim).cwiseAbs().maxCoeff());
}
//FullFlow
FullFlow::FullFlow()
{
  reset(false);
}
FullFlow::FullFlow(const string& fromFilename,const string& toFilename)
{
  //from
  boost::filesystem::path path(fromFilename);
  bool isPng=
    path.extension().string() == "png" ||
    path.extension().string() == "PNG" ||
    path.extension().string() == ".png" ||
    path.extension().string() == ".PNG";
  cv::Mat from=cv::imread(fromFilename,1);
  if(from.empty())
    printf("Cannot read image file: %s!\n",fromFilename.c_str());
  else copy(from,_from);
  //to
  cv::Mat to=cv::imread(toFilename,1);
  if(to.empty())
    printf("Cannot read image file: %s!\n",toFilename.c_str());
  else copy(to,_to);
  reset(isPng);
}
void FullFlow::createImageDesc(Eigen::MatrixXf& I,Eigen::MatrixXf IF[9])
{
  //writeImage(I,"initDesc.png");
  if(_presmooth>0)
    smooth(I,_presmooth);
  //writeImage(I,"preSmooth.png");
  Eigen::MatrixXf IX=gradX(I),IY=gradY(I);
  //writeImage(IX,"dx.png");
  //writeImage(IY,"dy.png");
  for(int i=0; i<8; i++) {
    HOG(IX,IY,IF[i],i,8);
    //writeImage(IF[i],"hog"+boost::lexical_cast<string>(i)+".png");
    if(_midsmooth>0)
      smooth(IF[i],_midsmooth);
    //writeImage(IF[i],"hogA"+boost::lexical_cast<string>(i)+".png");
    if(_hogSigmoid>0)
      sigmoid(IF[i],_hogSigmoid,0);
    //writeImage(IF[i],"hogB"+boost::lexical_cast<string>(i)+".png",255);
    if(_postsmooth>0)
      smooth(IF[i],_postsmooth);
    //writeImage(IF[i],"hogC"+boost::lexical_cast<string>(i)+".png",255);
  }
  IF[8]=Eigen::MatrixXf::Constant(I.rows(),I.cols(),_ninthDim);
}
void FullFlow::createImageDesc()
{
  int sz=4*(1<<_priorDownscale);
  int width=(_from.cols()/sz)*sz;
  int height=(_from.rows()/sz)*sz;

  _from=_from.block(0,0,height,width).eval();
  _to=_to.block(0,0,height,width).eval();

  createImageDesc(_from,_fromF);
  createImageDesc(_to,_toF);
}
void FullFlow::computePyramid()
{
    for(int j=0; j<9; j++) {
      for(int i=0; i<_priorDownscale; i++) {
        downSampleAvg(_fromF[j]);
        downSampleAvg(_toF[j]);
      }
      makePOT(_fromF[j]);
      makePOT(_toF[j]);
    }
  //for(int i=0; i<_priorDownscale; i++)
  //  for(int j=0; j<9; j++) {
  //    writeImage(_fromF[j],"hogFromDS"+boost::lexical_cast<string>(j)+".png",255);
  //    writeImage(_toF[j],"hogToDS"+boost::lexical_cast<string>(j)+".png",255);
  //  }
  _simLevel.clear();
  _simLevel.push_back(boost::shared_ptr<Level>(new Level(_fromF[0])));
  _simLevel.back()->computeSim(_fromF,_toF,_rectify);
  while(true) {
    boost::shared_ptr<Level> last=_simLevel.back();
    _simLevel.push_back(boost::shared_ptr<Level>(new Level(*last)));
    _simLevel.back()->computeSim(*(_simLevel[_simLevel.size()-2]),_rectify);
    if(_simLevel.back()->_sim.rows() == 1)
      break;
  }
}
float FullFlow::backtrace(int id,vector<Match>& correspondence) const
{
  float sumScore=0;
  correspondence.clear();
  correspondence.push_back(Match(0,id));
  for(int i=(int)_simLevel.size()-1; i>=1; i--)
    correspondence=_simLevel[i]->backtrace(*(_simLevel[i-1]),correspondence);
  Eigen::Vector2i offCtr=Eigen::Vector2i::Constant(((1<<_priorDownscale)*4)/2);
  for(int i=0; i<(int)correspondence.size(); i++) {
    Match& m=correspondence[i];
    m._pFrom=_simLevel[0]->getPatchCoord(m._r)*4*(1<<_priorDownscale)+offCtr;
    m._pTo=_simLevel[0]->getPixelCoord(m._c)*(1<<_priorDownscale)+offCtr;
    m._score=_simLevel[0]->_sim(m._r,m._c);
    sumScore+=m._score;
  }
  return sumScore;
}
void FullFlow::backtrace()
{
  float sumScore=0;
  vector<Match> corr;
  _correspondence.clear();
  for(int id=0;id<_simLevel.back()->_sim.cols();id++) {
    float score=backtrace(id,corr);
    printf("Found score: %f!\n",score);
    if(score > sumScore) {
      _correspondence=corr;
      sumScore=score;
    }
  }
  //clamp
  int i,fromSz=(int)_correspondence.size();
  sort(_correspondence.begin(),_correspondence.end());
  for(i=0; i<(int)_correspondence.size(); i++)
    if(_correspondence[i]._score > _clampThres*_correspondence.back()._score)
      break;
  _correspondence.erase(_correspondence.begin(),_correspondence.begin()+i);
  printf("Correspondence clamped from %d to %d!\n",fromSz,(int)_correspondence.size());
  //write
  drawMatch();
}
void FullFlow::drawMatch()
{
  cv::Mat imgFrom=writeImage(_from,1,true);
  cv::Mat imgTo=writeImage(_to,1,true);
  cv::Point dx(0,2),dy(2,0);
  cv::Scalar color;
  for(int i=0; i<(int)_correspondence.size(); i++) {
    //from
    const Eigen::Vector2i& pFrom=_correspondence[i]._pFrom;
    {
      hsv c;
      c.h=atan2(pFrom[0]-_from.rows()/2,pFrom[1]-_from.cols()/2);
      if(c.h < 0)
        c.h+=M_PI*2;
      c.h*=180.0f/M_PI;
      c.s=c.v=1;
      rgb crgb=hsv2rgb(c);
      color=cv::Scalar(crgb.b*255,crgb.g*255,crgb.r*255);
    }
    cv::Point pFromCV(pFrom[0],pFrom[1]);
    cv::line(imgFrom,pFromCV-dx,pFromCV+dx,color);
    cv::line(imgFrom,pFromCV-dy,pFromCV+dy,color);
    //to
    const Eigen::Vector2i& pTo=_correspondence[i]._pTo;
    cv::Point pToCV(pTo[0],pTo[1]);
    cv::line(imgTo,pToCV-dx,pToCV+dx,color);
    cv::line(imgTo,pToCV-dy,pToCV+dy,color);
  }
  cv::imwrite("fromMatch.png",imgFrom);
  cv::imwrite("toMatch.png",imgTo);
}
void FullFlow::solveOpticalFlow()
{
  createImageDesc();
  computePyramid();
  backtrace();
}
void FullFlow::writeMatch(const string& path)
{

}
void FullFlow::reset(bool isPng)
{
  _priorDownscale=1;
  _presmooth=isPng ? 0 : 1.0f;
  _midsmooth=1.5f;
  _postsmooth=1.0f;
  _hogSigmoid=0.2f;
  _ninthDim=isPng ? 0.1f : 0.3f;
  _rectify=1.6f;
  _clampThres=0.1f;
}
