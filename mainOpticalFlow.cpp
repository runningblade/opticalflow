#include <OpticalFlow/PostProcess.h>
#include <OpticalFlow/FullFlow.h>

#define TEST
int main()
{
#ifdef TEST0
  runPipeline("ambush",1);
  visualize("ambush",false);
#endif
#ifdef TEST1
  runPipeline("grimpe");
  visualize("grimpe",true);
#endif
#ifdef TEST2
  runPipeline("liberty");
  visualize("liberty",true);
#endif
  //FullFlow::Level::debugMatrixMultiply();
  //FullFlow::Level::debugMaxPooling();
  //FullFlow::Level::debugBacktrace();

  FullFlow ff("grimpe1.png","grimpe2.png");
  ff._priorDownscale=2;
  ff.solveOpticalFlow();

  //FullFlow ff("liberty1.png","liberty2.png");
  //ff.solveOpticalFlow();
  return 0;
}
